package Backend;
//Rodrigo Rivas
import java.util.Random;

public class RpsGame {
	private int wins=0;
	private int ties=0;
	private int losses=0;
	
	private Random rn = new Random();
	
	public int getWins() {
		return wins;
	}
	
	public int getTies() {
		return ties;
	}
	
	public int getLosses() {
		return losses;
	}
	
	public String playRound(String choice) {//rock
		String[] hand = {"rock","scissors","paper"};
		int randHand = rn.nextInt(3);//scissors
		
		if(choice.equals(hand[randHand])) {
			ties++;
			return "Computer plays " + hand[randHand] + " and the Player "+ choice +". It is a tie!";
		}
		else if(choice.equals(hand[0])&& hand[randHand].equals(hand[1]) || choice.equals(hand[1])&& hand[randHand].equals(hand[2]) || choice.equals(hand[2])&& hand[randHand].equals(hand[0])){
				wins++;
			return "Computer plays " + hand[randHand] + " and the Player "+ choice+". The Player wins";
		}
			losses++;
			return "Computer plays " + hand[randHand] + " and the Player "+choice+ ". The Computer wins";
		
		
	}
	
}
