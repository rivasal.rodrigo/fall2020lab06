package Gui;
//Rodrigo Rivas
import Backend.RpsGame;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {	
	private RpsGame game = new RpsGame();
	
	public void start(Stage stage) {
		Group root = new Group(); 

		//Buttons
		Button buttonRock = new Button("Rock");
		Button buttonPaper = new Button("Paper");
		Button buttonScissors = new Button("Scissors");
				
		//Horizontal 2
		TextField welcome = new TextField("Welcome! ");
		welcome.setPrefWidth(371.42);
		TextField wins = new TextField("Wins: " + game.getWins());
		wins.setPrefWidth(92.85);
		TextField losses = new TextField("Losses: "  + game.getLosses());
		losses.setPrefWidth(92.85);
		TextField ties = new TextField("Ties: "  + game.getTies());
		ties.setPrefWidth(92.85);
		
		//Buttons command
		RpsChoice rockChoice = new RpsChoice(welcome,wins,losses,ties,"rock",game);
		buttonRock.setOnAction(rockChoice);
				
		RpsChoice paperChoice = new RpsChoice(welcome,wins,losses,ties,"paper",game);
		buttonPaper.setOnAction(paperChoice);
				
		RpsChoice scissorsChoice = new RpsChoice(welcome,wins,losses,ties,"scissors",game);
		buttonScissors.setOnAction(scissorsChoice);
				
		// Horizontal Box for the buttons
		HBox buttons = new HBox();
		buttons.getChildren().addAll(buttonRock,buttonPaper,buttonScissors);
		
		//Horizontal Box 2 for the textFields
		HBox  textFields= new HBox();
		textFields.getChildren().addAll(welcome,wins,losses,ties);

		//vertical Box for the buttons and textFields
		VBox V1 = new VBox();		
		V1.getChildren().addAll(buttons,textFields);
		
		//Adding everything to the root where will be added to the scene after
		root.getChildren().add(V1);
		
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors by Rod Rivas"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    

