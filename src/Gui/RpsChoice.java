package Gui;
//Rodrigo Rivas
import Backend.RpsGame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

public class RpsChoice implements EventHandler<ActionEvent>{
	
	private TextField welcome;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String choice;
	private RpsGame game;
	
	public RpsChoice(TextField welcome,TextField wins,TextField losses,TextField ties,String choice,RpsGame game) {
		this.welcome = welcome;
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.choice = choice;
		this.game = game;
	}

	@Override
	public void handle(ActionEvent e) {
		//Handling all the textFields to show the result from the RpsGame class.
		String result = game.playRound(choice);
		welcome.setText(result);
		wins.setText("Wins: " + String.valueOf(game.getWins()));//
		losses.setText("Losses: "  +String.valueOf(game.getLosses()));
		ties.setText("Ties: " +String.valueOf(game.getTies()));
		
	}
	
}
